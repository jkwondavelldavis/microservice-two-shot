import React, {useEffect, useState } from "react"

function ShoeForm(){
    const [manufacturer, setManufacturer] = useState('')
    const [model_name, setModelName] = useState('')
    const [color, setColor] = useState('')
    const [picture_url, setPictureUrl] = useState('')
    const [bins, setBins] = useState([])
    const [bin, setBin] = useState('')

    const getBins = async () => {
        const url = "http://localhost:8100/api/bins/"
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json()
            setBins(data.bins)
        }
    }

    useEffect(()=>{
        getBins();
    },[]);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.manufacturer=manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin
        console.log(data)

        const shUrl = "http://localhost:8080/api/shoes/"
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(shUrl,fetchOptions)
    }

    const handleManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleModel = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePicture = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a shoe</h1>
              <form onSubmit={handleSubmit} id="create-bin-form">
                <div className="form-floating mb-3">
                  <input onChange={handleManufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                  <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleModel} placeholder="" required type="text" name="model_name" id="model_name" className="form-control"/>
                  <label htmlFor="model_name">Model name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleColor} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePicture} placeholder="Picture Url" required type="text" name="pictureUrl" id="pictureUrl" className="form-control"/>
                  <label htmlFor="pictureUrl">Picture Url</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleBin} required name="bin" id="bin" className="form-select">
                    <option value="">Choose a bin</option>
                    {bins.map(bin => {
                        return(
                            <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                        )
                    })}

                  </select>
                </div>
                <button className="btn btn-primary">Add</button>
              </form>
            </div>
          </div>
        </div>
    );
}

export default ShoeForm
