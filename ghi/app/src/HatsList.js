import React, { useEffect, useState } from "react";
import { redirect } from "react-router-dom";
import { Link } from "react-router-dom";

// fetch data and make make html

function HatsList() {
    const [hats, setHats] = useState([]);

    async function fetchHats() {
        const response = await fetch("http://localhost:8090/api/hats/")

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
            console.log(data)
        }
    }


    async function deleteHat(href) {
        const response = await fetch("http://localhost:8090" + href, { method: "DELETE" },)
        window.location.reload()
    }

    useEffect(() => {
        fetchHats();
    }, []);

    return (
        <table className="table table-hover table-striped ">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style Name</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td>{hat.fabric}</td>
                            <td>{hat.style_name}</td>
                            <td><button className="btn btn-danger" onClick={() => { deleteHat(hat.href) }}>Delete</button></td>
                            <td>
                                <Link to={`${hat.id}`}>
                                <button className="btn btn-primary"> Details</button>
                                </Link>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default HatsList;