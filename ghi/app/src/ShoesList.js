import React, {useEffect, useState } from "react"
import { Link } from 'react-router-dom';



async function deleteShoe(href) {
  const response = await fetch("http://localhost:8080" + href, { method: "DELETE" },)
  window.location.reload()
}

function ShoesList() {
    const [shoes, setShoes] = useState([])

    async function loadShoes() {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
          const data = await response.json();
          setShoes(data.shoes)
        } else {
          console.error(response);
        }
      }

    useEffect(()=>{
        loadShoes();
    }, []);

    return (
      <div className="px-4 my-4 text-center">
        <h1 className="display-5 fw-bold">Shoes!</h1>
        <div className="col-lg-6 mx-auto">
          <table className="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe =>{
                    return(
                        <tr key={shoe.href}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>
                              <Link to={`${shoe.href.split('/')[3]}`}>
                                <button className="btn btn-primary"> Details</button>
                              </Link>
                            </td>
                            <td>
                              <button className="btn btn-danger" onClick={() => { deleteShoe(shoe.href) }}>
                                Delete
                              </button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }

  export default ShoesList;
