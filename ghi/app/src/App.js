import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatForm from './HatForm';
import HatDetails from './HatDetails';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import ShoeDetails from './ShoeDetails';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container-fluid">
        <Routes>
          <Route path="" element={<MainPage />} />
        </Routes>
        <Routes>
          <Route path="hats">
            <Route path="" element={<HatsList />} />
            <Route path="new" element={<HatForm />} />
            <Route path=":id" element={<HatDetails />}></Route>
            <Route path=""></Route>
          </Route>
          <Route path="shoes">
            <Route path="" element={<ShoesList />} />
            <Route path="new" element={<ShoeForm />} />
            <Route path=":id" element={<ShoeDetails />}></Route>
          </Route>

        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
