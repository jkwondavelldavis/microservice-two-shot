import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";

export default function HatDetails() {
    const { id } = useParams();
    const [shoe, setShoe] = useState("");

    async function fetchShoe() {
        const response = await fetch("http://localhost:8080/api/shoes/" + id)
        if (response.ok) {
            const data = await response.json();
            setShoe(data.shoe)
            console.log(data)
        }
    }

    useEffect(() => {
        fetchShoe();
    }, "");

    return (
        <table className="table table-hover table-striped ">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Picture Url</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{shoe.manufacturer}</td>
                    <td>{shoe.model_name}</td>
                    <td>{shoe.color}</td>
                    <td><a href={shoe.picture_url}>Click Image</a></td>
                </tr>
            </tbody>
        </table>
    )
}
