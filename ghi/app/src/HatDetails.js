import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";

export default function HatDetails() {
    const { id } = useParams();
    const [hat, setHat] = useState("");

    async function fetchhat() {
        const response = await fetch("http://localhost:8090/api/hats/" + id)
        console.log(response)

        if (response.ok) {
            const data = await response.json();
            setHat(data)
            console.log(data)

        }
    }

    useEffect(() => {
        fetchhat();
    }, "");





    return (
        <table className="table table-hover table-striped ">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Picture Url</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{hat.fabric}</td>
                    <td>{hat.style_name}</td>
                    <td>{hat.color}</td>
                    <td><a href={hat.picture_url}>Click Image</a></td>
                </tr>
            </tbody>
        </table>
    )
}
