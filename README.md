# Wardrobify

Team:

* Davis Jkwon - Hats
* John - Shoes microservice

## Design
We will be using bootstrap.


## Shoes microservice

Shoes microservice will have a shoe model which will track a manufacturer, model name, color,
a picture url, and a bin. It will also have a BinVO model which will pull data from the wardrobe microservice Bin model.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

I will be making a Hats model to get hats input from user.
I will also make a LocationVO model to get location information for my Hat's model.
I will be using polling to inegrate the Location model and my LocationVO model.
